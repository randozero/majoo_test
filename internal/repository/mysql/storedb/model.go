package storedb

type GetOmzetMerchantByMerchantIDRequest struct {
	MerchantID int32
	From       string
	To         string
	Page       int32
	Limit      int32
}

type GetOmzetMerchantByMerchantIDResponse struct {
	Tanggal string
	Total   float64
}

type GetOmzetMerchantByMerchantIDAndOutletIDRequest struct {
	MerchantID int32
	OutletID   int32
	From       string
	To         string
	Page       int32
	Limit      int32
}

type GetOmzetMerchantByMerchantIDAndOutletIDResponse struct {
	Tanggal string
	Total   float64
}

type GetUserDataByLoginRequest struct {
	Username string
	Password string
}

type GetUserDataByLoginResponse struct {
	ID   int
	Name string
}

type GetMerchantDataByMerchantIDAndUserIDRequest struct {
	MerchantID int
	UserID     int
}

type GetMerchantDataByMerchantIDAndUserIDResponse struct {
	Name string
}

type GetMerchantsDataByUserIdResponse struct {
	ID   int
	Name string
}

type GetOutletDataResponse struct {
	ID   int
	Name string
}

type GetOutletsByOutletIdAndMerchantIDRequest struct {
	OutletID   int
	MerchantID int
}
