package storedb

import "context"

const getOmzetMerchantByMerchantID = `
SELECT DATE_FORMAT(cal.date, '%d-%m-%Y') tanggal, ifnull(sum(trx.bill_total), 0) total
FROM (
      SELECT SUBDATE(STR_TO_DATE(?, '%d-%m-%Y') , INTERVAL 0 DAY) + INTERVAL xc DAY AS date
      FROM (
            SELECT @xi:=@xi+1 as xc from
            (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) xc1,
            (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) xc2,
            (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) xc3,
            (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) xc4,
            (SELECT @xi:=-1) xc0
      ) xxc1
) cal
LEFT JOIN transactions trx ON DATE(cal.date) = DATE(trx.created_at) and trx.merchant_id = ?
WHERE cal.date <= STR_TO_DATE(?, '%d-%m-%Y')
group by tanggal
ORDER BY tanggal
limit ?, ?;
`

func (q Queries) GetOmzetMerchantByMerchantID(
	ctx context.Context,
	request GetOmzetMerchantByMerchantIDRequest) (
	[]GetOmzetMerchantByMerchantIDResponse,
	error,
) {
	rows, err := q.db.QueryContext(ctx,
		getOmzetMerchantByMerchantID,
		request.From, request.MerchantID, request.To, request.Page*request.Limit, request.Limit,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := make([]GetOmzetMerchantByMerchantIDResponse, 0)
	for rows.Next() {
		var i GetOmzetMerchantByMerchantIDResponse
		rows.Scan(
			&i.Tanggal,
			&i.Total,
		)
		items = append(items, i)
	}
	return items, nil
}

const getOmzetMerchantByMerchantIDAndOutletID = `
SELECT DATE_FORMAT(cal.date, '%d-%m-%Y') tanggal, ifnull(sum(trx.bill_total), 0) total
FROM (
      SELECT SUBDATE(STR_TO_DATE(?, '%d-%m-%Y') , INTERVAL 0 DAY) + INTERVAL xc DAY AS date
      FROM (
            SELECT @xi:=@xi+1 as xc from
            (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) xc1,
            (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) xc2,
            (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) xc3,
            (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) xc4,
            (SELECT @xi:=-1) xc0
      ) xxc1
) cal
LEFT JOIN transactions trx ON DATE(cal.date) = DATE(trx.created_at) and trx.merchant_id = ? and trx.outlet_id = ?
WHERE cal.date <= STR_TO_DATE(?, '%d-%m-%Y')
group by tanggal
ORDER BY tanggal
limit ?, ?;
`

func (q Queries) GetOmzetMerchantByMerchantIDAndOutletID(
	ctx context.Context,
	request GetOmzetMerchantByMerchantIDAndOutletIDRequest) (
	[]GetOmzetMerchantByMerchantIDAndOutletIDResponse,
	error,
) {
	rows, err := q.db.QueryContext(ctx,
		getOmzetMerchantByMerchantIDAndOutletID,
		request.From, request.MerchantID, request.OutletID, request.To, request.Page*request.Limit, request.Limit,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := make([]GetOmzetMerchantByMerchantIDAndOutletIDResponse, 0)
	for rows.Next() {
		var i GetOmzetMerchantByMerchantIDAndOutletIDResponse
		rows.Scan(
			&i.Tanggal,
			&i.Total,
		)
		items = append(items, i)
	}
	return items, nil
}

const getUserDataByLogin = `
select 
	id, name 
from users
where
	user_name = ? and password = ?
limit 1;
`

func (q Queries) GetUserDataByLogin(
	ctx context.Context,
	request GetUserDataByLoginRequest) (
	GetUserDataByLoginResponse,
	error,
) {
	row := q.db.QueryRowContext(ctx, getUserDataByLogin,
		request.Username, request.Password,
	)
	var item GetUserDataByLoginResponse
	err := row.Scan(
		&item.ID,
		&item.Name,
	)
	return item, err
}

const getMerchantsDataByUserID = `
select 
	id,
	merchant_name 
from merchants 
where 
	user_id = ?
order by id;
`

func (q Queries) GetMerchantsDataByUserID(
	ctx context.Context,
	userID int) (
	[]GetMerchantsDataByUserIdResponse,
	error,
) {
	rows, err := q.db.QueryContext(ctx, getMerchantsDataByUserID,
		userID,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := make([]GetMerchantsDataByUserIdResponse, 0)
	for rows.Next() {
		var i GetMerchantsDataByUserIdResponse
		rows.Scan(
			&i.ID,
			&i.Name,
		)
		items = append(items, i)
	}
	return items, err
}

const getMerchantDataByMerchantIDAndUserID = `
select 
	merchant_name 
from merchants 
where 
	id = ? and user_id = ?
limit 1;
`

func (q Queries) GetMerchantDataByMerchantIDAndUserID(
	ctx context.Context,
	request GetMerchantDataByMerchantIDAndUserIDRequest) (
	GetMerchantDataByMerchantIDAndUserIDResponse,
	error,
) {
	row := q.db.QueryRowContext(ctx, getMerchantDataByMerchantIDAndUserID,
		request.MerchantID, request.UserID,
	)
	var item GetMerchantDataByMerchantIDAndUserIDResponse
	err := row.Scan(
		&item.Name,
	)
	return item, err
}

const getOutletsByMerchantID = `
select 
	id, outlet_name 
from outlets 
where 
	merchant_id = ?
order by id;
`

func (q Queries) GetOutletsByMerchantID(
	ctx context.Context,
	merchantID int) (
	[]GetOutletDataResponse,
	error,
) {
	rows, err := q.db.QueryContext(ctx, getOutletsByMerchantID,
		merchantID,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := make([]GetOutletDataResponse, 0)
	for rows.Next() {
		var i GetOutletDataResponse
		rows.Scan(
			&i.ID,
			&i.Name,
		)
		items = append(items, i)
	}
	return items, err
}

const getOutletByOutletIDAndMerchantID = `
select 
	id, outlet_name 
from outlets 
where
	id = ? and
	merchant_id = ?
order by id
limit 1;
`

func (q Queries) GetOutletByOutletIDAndMerchantID(
	ctx context.Context,
	request GetOutletsByOutletIdAndMerchantIDRequest) (
	GetOutletDataResponse,
	error,
) {
	row := q.db.QueryRowContext(ctx, getOutletByOutletIDAndMerchantID,
		request.OutletID, request.MerchantID,
	)
	var item GetOutletDataResponse
	err := row.Scan(
		&item.ID,
		&item.Name,
	)
	return item, err
}
