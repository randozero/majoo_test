package mysql_kit

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
)

func CreateDefaultMySQLConnection() *sql.DB {
	username := viper.GetString("db.username")
	password := viper.GetString("db.password")
	host := viper.GetString("db.host")
	port := viper.GetString("db.port")
	database := viper.GetString("db.database")

	connectionPath := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s",
		username,
		password,
		host,
		port,
		database,
	)
	db, err := sql.Open("mysql", connectionPath)
	if err != nil {
		log.Fatalf("fail to create default db connection, %s", err.Error())
	}
	var test int
	row := db.QueryRow("select 1")
	if err := row.Scan(&test); err != nil {
		log.Fatalf("fail to connect default db, %s", err.Error())
	}
	log.Println("default db connection OK.")
	return db
}
