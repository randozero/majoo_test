package viper_kit

import (
	"log"

	"github.com/spf13/viper"
)

func LoadConfig(path string) {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(path)

	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("fail to load config, %s", err.Error())
	}
	log.Println("Load config OK.")
}
