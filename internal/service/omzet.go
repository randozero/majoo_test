package service

import (
	"context"
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/randozero/majoo_test/internal/repository/mysql/storedb"
	"gitlab.com/randozero/majoo_test/pkg/rest_model"
)

type OmzetService struct {
	storeDB *sql.DB
}

func NewOmzetService(storeDB *sql.DB) *OmzetService {
	return &OmzetService{
		storeDB: storeDB,
	}
}

const format_date = "02-01-2006"

func (s *OmzetService) GetOmzetMerchant(ctx context.Context, merchantID int32, month, year, limit, page int) (*rest_model.MerchantOmzetData, error) {
	t := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
	fr := t.Format(format_date)
	lastDay := t.AddDate(0, 1, -1)
	to := lastDay.Format(format_date)
	query := storedb.New(s.storeDB)
	data, err := query.GetOmzetMerchantByMerchantID(ctx, storedb.GetOmzetMerchantByMerchantIDRequest{
		MerchantID: merchantID,
		From:       fr,
		To:         to,
		Page:       int32(page),
		Limit:      int32(limit),
	})
	if err != nil {
		return nil, errors.Wrapf(err, "fail to get GetOmzetMerchantByMerchantID, merchantId : %d, month : %d, year : %d",
			merchantID,
			month,
			year,
		)
	}
	response := &rest_model.MerchantOmzetData{
		Merchant: &rest_model.MerchantData{},
		Outlets:  make([]*rest_model.OutletData, 0),
		Omzets:   make([]*rest_model.OmzetData, 0),
	}
	for _, v := range data {
		response.Omzets = append(response.Omzets, &rest_model.OmzetData{
			Tanggal: v.Tanggal,
			Omzet:   v.Total,
		})
	}
	return response, nil
}

func (s *OmzetService) GetOmzetMerchantOutlet(ctx context.Context, merchantID, outletID int32, month, year, limit, page int) (*rest_model.MerchantOutletOmzetData, error) {
	t := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
	fr := t.Format(format_date)
	lastDay := t.AddDate(0, 1, -1)
	to := lastDay.Format(format_date)
	query := storedb.New(s.storeDB)
	data, err := query.GetOmzetMerchantByMerchantIDAndOutletID(ctx, storedb.GetOmzetMerchantByMerchantIDAndOutletIDRequest{
		MerchantID: merchantID,
		OutletID:   outletID,
		From:       fr,
		To:         to,
		Page:       int32(page),
		Limit:      int32(limit),
	})
	if err != nil {
		return nil, errors.Wrapf(err, "fail to get GetOmzetMerchantByMerchantIDAndOutletID, merchantId : %d, outletID, month : %d, year : %d",
			merchantID,
			outletID,
			month,
			year,
		)
	}
	response := &rest_model.MerchantOutletOmzetData{
		Merchant: &rest_model.MerchantData{},
		Outlet:   &rest_model.OutletData{},
		Omzets:   make([]*rest_model.OmzetData, 0),
	}
	for _, v := range data {
		response.Omzets = append(response.Omzets, &rest_model.OmzetData{
			Tanggal: v.Tanggal,
			Omzet:   v.Total,
		})
	}
	return response, nil
}
