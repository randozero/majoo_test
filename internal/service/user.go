package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/spf13/viper"
	"gitlab.com/randozero/majoo_test/internal/model"
	"gitlab.com/randozero/majoo_test/internal/repository/mysql/storedb"
	"gitlab.com/randozero/majoo_test/pkg/rest_model"
)

type UserService struct {
	storeDB *sql.DB
}

func NewUserService(storeDB *sql.DB) *UserService {
	return &UserService{
		storeDB: storeDB,
	}
}

func (u *UserService) GetLoginToken(ctx context.Context, username, password string) (*rest_model.LoginResponse, error) {
	query := storedb.New(u.storeDB)
	data, err := query.GetUserDataByLogin(ctx, storedb.GetUserDataByLoginRequest{
		Username: username, Password: password,
	})
	if err != nil {
		return nil, err
	}
	claims := model.NewJWTCustomClaims(data.Name, data.ID, time.Now().Add(viper.GetDuration("token.expired")).Unix())
	t := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := t.SignedString([]byte(viper.GetString("token.access-secret")))
	if err != nil {
		return nil, err
	}
	return &rest_model.LoginResponse{
		Type:  "Bearer",
		Token: token,
	}, nil
}

func (u UserService) GetMerchantData(ctx context.Context, merchantID, UserID int) (*rest_model.MerchantData, error) {
	query := storedb.New(u.storeDB)
	data, err := query.GetMerchantDataByMerchantIDAndUserID(ctx, storedb.GetMerchantDataByMerchantIDAndUserIDRequest{
		MerchantID: merchantID, UserID: UserID,
	})
	if err != nil {
		return nil, err
	}
	return &rest_model.MerchantData{
		MerchantID:   merchantID,
		MerchantName: data.Name,
	}, nil
}

func (u UserService) GetOutletsData(ctx context.Context, merchantID int) ([]*rest_model.OutletData, error) {
	query := storedb.New(u.storeDB)
	data, err := query.GetOutletsByMerchantID(ctx, merchantID)
	if err != nil {
		return nil, err
	}
	response := make([]*rest_model.OutletData, 0)
	for _, v := range data {
		response = append(response, &rest_model.OutletData{
			OutletID:   v.ID,
			OutletName: v.Name,
		})
	}
	return response, nil
}

func (u UserService) GetSingleOutletData(ctx context.Context, outletID, merchantID int) (*rest_model.OutletData, error) {
	query := storedb.New(u.storeDB)
	data, err := query.GetOutletByOutletIDAndMerchantID(ctx, storedb.GetOutletsByOutletIdAndMerchantIDRequest{
		OutletID:   outletID,
		MerchantID: merchantID,
	})
	if err != nil {
		return nil, err
	}
	return &rest_model.OutletData{
		OutletID:   data.ID,
		OutletName: data.Name,
	}, nil
}

func (u UserService) GetMerchantsData(ctx context.Context, userID int) ([]*rest_model.MerchantData, error) {
	query := storedb.New(u.storeDB)
	data, err := query.GetMerchantsDataByUserID(ctx, userID)
	response := make([]*rest_model.MerchantData, 0)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return response, nil
		}
		return nil, err
	}
	for _, v := range data {
		response = append(response, &rest_model.MerchantData{
			MerchantID:   v.ID,
			MerchantName: v.Name,
		})
	}
	return response, nil
}
