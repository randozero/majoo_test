package model

import "github.com/golang-jwt/jwt"

type JWTCustomClaims struct {
	Name string `json:"name"`
	ID   int    `json:"id"`
	jwt.StandardClaims
}

func NewJWTCustomClaims(name string, ID int, unixTime int64) *JWTCustomClaims {
	return &JWTCustomClaims{
		name, ID, jwt.StandardClaims{
			ExpiresAt: unixTime,
		},
	}
}
