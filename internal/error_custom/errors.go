package error_custom

var (
	ErrMonthFilter      = "Month parameter min value = 1 and max value = 12"
	ErrInvalidParameter = "Request parameter is invalid"
	ErrUserNotExist     = "Username or Password wrong"
)
