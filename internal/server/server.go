package server

import (
	"database/sql"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/randozero/majoo_test/internal/server/controller"
)

type ServerContainer struct {
	handlerSvc controller.HandlerService
}

func CreateNewServerContainer(storeDB *sql.DB) ServerContainer {
	return ServerContainer{
		handlerSvc: controller.NewHandlerService(storeDB),
	}
}

func (s ServerContainer) StartServer() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	s.routingRestPath(e)

	s.listingRoutes(e)

	e.HideBanner = true

	e.Logger.Fatal(e.Start(":8080"))
}
