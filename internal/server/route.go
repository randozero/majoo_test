package server

import (
	"fmt"
	"log"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
	"gitlab.com/randozero/majoo_test/internal/model"
)

func (s ServerContainer) routingRestPath(e *echo.Echo) {
	userGr := e.Group("/user")
	userGr.POST("/login", s.handlerSvc.GetLoginDataHandler)

	jwtConfig := middleware.JWTConfig{
		Claims:     &model.JWTCustomClaims{},
		SigningKey: []byte(viper.GetString("token.access-secret")),
	}
	JwtFilter := middleware.JWTWithConfig(jwtConfig)
	merchantGR := e.Group("/merchant")
	merchantGR.GET("/list", s.handlerSvc.GetMerchantsDataHandler, JwtFilter)
	merchantGR.GET("/:ID/:month-year", s.handlerSvc.GetOmzetMerchantHandler, JwtFilter)

	outletGR := merchantGR.Group("/:ID/outlet")
	outletGR.GET("/:outletID/:month-year", s.handlerSvc.GetOmzetMerchantOutletHandler, JwtFilter)
}

func (s ServerContainer) listingRoutes(e *echo.Echo) {
	routes := e.Routes()
	log.Println("Listing online path :")
	for _, v := range routes {
		log.Println(fmt.Sprintf("%s %s", v.Method, v.Path))
	}
	log.Println("Rest service ready.")
}
