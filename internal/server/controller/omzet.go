package controller

import (
	"database/sql"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/randozero/majoo_test/internal/error_custom"
	"gitlab.com/randozero/majoo_test/internal/model"
	"gitlab.com/randozero/majoo_test/pkg/rest_model"
)

const MonthYearPattern = "01-2006"

// GetOmzetMerchantHandler
// Get "merchant/:ID/:month-year"
// Example merchant/1/11-2021
func (h HandlerService) GetOmzetMerchantHandler(e echo.Context) error {
	var requestBinding struct {
		merchantID int32
		monthYear  string
	}
	user := e.Get("user").(*jwt.Token)
	claims := user.Claims.(*model.JWTCustomClaims)
	if err := echo.PathParamsBinder(e).
		Int32("ID", &requestBinding.merchantID).
		String("month-year", &requestBinding.monthYear).
		BindError(); err != nil {
		return e.JSON(http.StatusBadRequest, &rest_model.APIErrorResponse{
			Code:    http.StatusBadRequest,
			Message: error_custom.ErrInvalidParameter,
		})
	}
	t, err := time.Parse(MonthYearPattern, requestBinding.monthYear)
	if err != nil {
		return e.JSON(http.StatusBadRequest, &rest_model.APIErrorResponse{
			Code:    http.StatusBadRequest,
			Message: error_custom.ErrInvalidParameter,
		})
	}
	merchantData, err := h.userSvc.GetMerchantData(e.Request().Context(), int(requestBinding.merchantID), claims.ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return e.JSON(http.StatusNotFound, &rest_model.APIErrorResponse{
				Code:    http.StatusNotFound,
				Message: "user didn't have this merchant",
			})
		}
		return err
	}
	outletsData, err := h.userSvc.GetOutletsData(e.Request().Context(), int(requestBinding.merchantID))
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return err
		}
	}
	limit := 10
	page := 0
	if err := echo.QueryParamsBinder(e).
		Int("limit", &limit).
		Int("page", &page).
		BindError(); err != nil {
		return errors.Wrapf(err, "invalid limit or page")
	}
	if limit <= 0 {
		limit = 10
	}
	if page > 0 {
		page--
	} else if page < 0 {
		page = 0
	}
	response, err := h.omzetSvc.GetOmzetMerchant(e.Request().Context(),
		requestBinding.merchantID,
		int(t.Month()), t.Year(),
		limit, page,
	)
	if err != nil {
		return errors.Wrapf(err, "GetOmzetMerchant")
	}
	response.Merchant = merchantData
	if outletsData != nil {
		response.Outlets = outletsData
	}
	return e.JSON(http.StatusOK, response)
}

// GetOmzetMerchantOutletHandler
// Get "merchant/:ID/outlet/:outletID/:month-year"
// Example merchant/1/11-2021
func (h HandlerService) GetOmzetMerchantOutletHandler(e echo.Context) error {
	var requestBinding struct {
		merchantID int32
		outletID   int32
		monthYear  string
	}
	user := e.Get("user").(*jwt.Token)
	claims := user.Claims.(*model.JWTCustomClaims)
	if err := echo.PathParamsBinder(e).
		Int32("ID", &requestBinding.merchantID).
		Int32("outletID", &requestBinding.outletID).
		String("month-year", &requestBinding.monthYear).
		BindError(); err != nil {
		return e.JSON(http.StatusBadRequest, &rest_model.APIErrorResponse{
			Code:    http.StatusBadRequest,
			Message: error_custom.ErrInvalidParameter,
		})
	}
	t, err := time.Parse(MonthYearPattern, requestBinding.monthYear)
	if err != nil {
		return e.JSON(http.StatusBadRequest, &rest_model.APIErrorResponse{
			Code:    http.StatusBadRequest,
			Message: error_custom.ErrInvalidParameter,
		})
	}
	merchantData, err := h.userSvc.GetMerchantData(e.Request().Context(), int(requestBinding.merchantID), claims.ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return e.JSON(http.StatusNotFound, &rest_model.APIErrorResponse{
				Code:    http.StatusNotFound,
				Message: "user didn't have this merchant",
			})
		}
		return err
	}
	outletData, err := h.userSvc.GetSingleOutletData(e.Request().Context(), int(requestBinding.outletID), int(requestBinding.merchantID))
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return e.JSON(http.StatusNotFound, &rest_model.APIErrorResponse{
				Code:    http.StatusNotFound,
				Message: "merchant didn't have this outlet",
			})
		}
		return err
	}
	limit := 10
	page := 0
	if err := echo.QueryParamsBinder(e).
		Int("limit", &limit).
		Int("page", &page).
		BindError(); err != nil {
		return e.JSON(http.StatusBadRequest, &rest_model.APIErrorResponse{
			Code:    http.StatusBadRequest,
			Message: error_custom.ErrInvalidParameter,
		})
	}
	if limit <= 0 {
		limit = 10
	}
	if page > 0 {
		page--
	} else if page < 0 {
		page = 0
	}
	response, err := h.omzetSvc.GetOmzetMerchantOutlet(e.Request().Context(),
		requestBinding.merchantID,
		requestBinding.outletID,
		int(t.Month()), t.Year(),
		limit, page,
	)
	if err != nil {
		return errors.Wrapf(err, "GetOmzetMerchant")
	}
	response.Merchant = merchantData
	response.Outlet = outletData
	return e.JSON(http.StatusOK, response)
}
