package controller

import (
	"database/sql"
	"net/http"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/randozero/majoo_test/internal/error_custom"
	"gitlab.com/randozero/majoo_test/internal/model"
	"gitlab.com/randozero/majoo_test/pkg/rest_model"
)

func (h HandlerService) GetLoginDataHandler(e echo.Context) error {
	var request rest_model.LoginRequest
	if err := e.Bind(&request); err != nil {
		return e.JSON(http.StatusBadRequest, &rest_model.APIErrorResponse{
			Code:    http.StatusBadRequest,
			Message: error_custom.ErrInvalidParameter,
		})
	}
	response, err := h.userSvc.GetLoginToken(e.Request().Context(), request.Username, request.Password)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return e.JSON(http.StatusBadRequest, &rest_model.APIErrorResponse{
				Code:    http.StatusBadRequest,
				Message: error_custom.ErrUserNotExist,
			})
		}
		return err
	}
	return e.JSON(http.StatusOK, response)
}

func (h HandlerService) GetMerchantsDataHandler(e echo.Context) error {
	user := e.Get("user").(*jwt.Token)
	claims := user.Claims.(*model.JWTCustomClaims)
	response, err := h.userSvc.GetMerchantsData(e.Request().Context(), claims.ID)
	if err != nil {
		return err
	}
	return e.JSON(http.StatusOK, response)
}
