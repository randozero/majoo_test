package controller

import (
	"database/sql"

	"gitlab.com/randozero/majoo_test/internal/service"
)

type HandlerService struct {
	omzetSvc *service.OmzetService
	userSvc  *service.UserService
}

func NewHandlerService(storeDB *sql.DB) HandlerService {
	return HandlerService{
		omzetSvc: service.NewOmzetService(storeDB),
		userSvc:  service.NewUserService(storeDB),
	}
}
