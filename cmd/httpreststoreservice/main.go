package main

import (
	"gitlab.com/randozero/majoo_test/internal/config/mysql_kit"
	"gitlab.com/randozero/majoo_test/internal/config/viper_kit"
	"gitlab.com/randozero/majoo_test/internal/server"
)

func main() {
	viper_kit.LoadConfig("../../")
	storeDB := mysql_kit.CreateDefaultMySQLConnection()

	sv := server.CreateNewServerContainer(storeDB)

	sv.StartServer()
}
