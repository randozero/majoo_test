package rest_model

type OmzetData struct {
	Tanggal string  `json:"tanggal"`
	Omzet   float64 `json:"omzet"`
}

type MerchantOmzetData struct {
	Merchant *MerchantData `json:"merchant"`
	Outlets  []*OutletData `json:"outlets"`
	Omzets   []*OmzetData  `json:"omzets"`
}

type MerchantData struct {
	MerchantID   int    `json:"merchantId"`
	MerchantName string `json:"merchantName"`
}

type OutletData struct {
	OutletID   int    `json:"outletId"`
	OutletName string `json:"OutletName"`
}

type MerchantOutletOmzetData struct {
	Merchant *MerchantData `json:"merchant"`
	Outlet   *OutletData   `json:"outlet"`
	Omzets   []*OmzetData  `json:"omzets"`
}

type APIErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type LoginResponse struct {
	Type  string `json:"type"`
	Token string `json:"token"`
}
