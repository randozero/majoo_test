module gitlab.com/randozero/majoo_test

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/labstack/echo/v4 v4.7.2
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.10.1
)
